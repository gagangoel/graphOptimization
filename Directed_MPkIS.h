#ifndef DIRECTED_MPKIS_INCLUDED
#define DIRECTED_MPKIS_INCLUDED

#include "K_G_Foliage.h"

using namespace std;
using namespace boost;

vector<graph_traits<digraph>::edge_descriptor> _Directed_MPkIS(digraph g, graph_traits<digraph>::vertex_descriptor root, int connectivity)
{
	bool inserted;
	property_map<digraph, edge_weight_t>::type weight_map = get(edge_weight, g);

   /* Changing edge costs of the input directed graph so that the final solution of Minimum
   Cost Spanning Subgraph Algorithm is the Minimum Power Spanning Subgraph */

   graph_traits<digraph>::vertex_iterator vi, vi_end;
   graph_traits<digraph>::out_edge_iterator out_ei, out_ei_end;
   typedef graph_traits<digraph>::vertex_descriptor vd;
   graph_traits<digraph>::edge_descriptor ed;
   vd source1, target1;
   double power;
   for (boost::tie(vi, vi_end) = vertices(g); vi!=vi_end; vi++)
   {
	   vector<double> temp_weight;
	   for (boost::tie(out_ei, out_ei_end) = out_edges(*vi, g); out_ei!=out_ei_end; out_ei++)
	   {
			source1 = source(*out_ei, g);
			target1 = target(*out_ei, g);
			boost::tie(ed, inserted) = edge(source1, target1, g);
			temp_weight.push_back(weight_map[ed]);
	   }
	   std::sort(temp_weight.begin(), temp_weight.end());
	   /* power computes the Power of every vertex of Min-Power Subgraph with: (out)degree of every vertex = connectivity */
	   power = temp_weight[connectivity - 1];
	   for (boost::tie(out_ei, out_ei_end) = out_edges(*vi, g); out_ei!=out_ei_end; out_ei++)
	   {
			source1 = source(*out_ei, g);
			target1 = target(*out_ei, g);
			boost::tie(ed, inserted) = edge(source1, target1, g);
			weight_map[ed] = std::max((weight_map[ed] - power), 0.0);
	   }
   }

    /* K-G-Foliage returns the MCkIS by reversing Graph edges passed as input to the k-g-foliage (MCkOS) and 
	finally reversing edges of the solution returned by k-g-foliage to obtain MCkIS subgraphs */

   /* Reversing All Edges of the Directed Graph */
   digraph g_reverse;
   graph_traits<digraph>::edge_iterator ei, ei_end;
   for (boost::tie(ei, ei_end) = edges(g); ei != ei_end; ei++)
   {
	   source1 = source(*ei, g);
	   target1 = target(*ei, g);
	   boost::tie(ed, inserted) = edge (source1, target1, g);
	   if (inserted)
	   {
		   add_edge(target1, source1, g_reverse);
	   }
   }

   vector<vector<graph_traits<digraph>::edge_descriptor>> MCKOS =  k_g_foliage(g_reverse, root, connectivity);
   //cout<<"Total number of k-g foliages are: "<<MCKOS.size()<<endl;

   if (int(MCKOS.size()) == 0)
   {
	   vector<graph_traits<digraph>::edge_descriptor> empty_MCKIS;
	   return empty_MCKIS;
   }
   
    ofstream fout3 ("MCKOS.txt");
  	for (size_t i = 0; i < MCKOS.size(); i++)
	{
		for (size_t j = 0; j < MCKOS[i].size(); j++)
			fout3<<MCKOS[i][j]<<" ";
		fout3<<endl;
	}
	fout3.close();

   /* Again Reversing all edges of the solution of k_g_foliage to obtain desired MCkIS subgraphs */
   digraph g_temp;
   graph_traits<digraph>::edge_descriptor ed_temp;
   bool inserted_temp;
   vector<vector<graph_traits<digraph>::edge_descriptor>> MCKIS;
   int index = 0;
   for (size_t i = 0; i < MCKOS.size(); i++)
   {
	   MCKIS.push_back(vector<graph_traits<digraph>::edge_descriptor>());
	   for (size_t j = 0; j < MCKOS[i].size(); j++)
	   {
		   source1 = source(MCKOS[i][j], g_reverse);
		   target1 = target(MCKOS[i][j], g_reverse);
		   boost::tie(ed, inserted) = edge (source1, target1, g_reverse);
		   if (inserted)
		   {
			   boost::tie(ed_temp, inserted_temp) = add_edge(target1, source1, g_temp);
			   MCKIS[index].push_back(ed_temp);
		   }
	   }
	   index++;
   }

   	ofstream fout4 ("MCKIS.txt");
	for (size_t i = 0; i < MCKIS.size(); i++)
	{
		for (size_t j = 0; j < MCKIS[i].size(); j++)
			fout4<<MCKIS[i][j]<<" ";
		fout4<<endl;
	}
	fout4.close();

	/* Now computing the desired MPkIS from MCkIS subgraphs */
	vector<double> sum_weights;
	double temp = 0;
	for(size_t i = 0; i < MCKIS.size(); i++)
	{
		temp = 0;
		for (size_t j = 0; j < MCKIS[i].size(); j++)
		{
			source1 = source(MCKIS[i][j], g);
			target1 = target(MCKIS[i][j], g);
			boost::tie(ed, inserted) = edge (source1, target1, g);
			if (inserted)
			{
				temp += weight_map[ed];
			}
		}
		sum_weights.push_back(temp);
	}
	int it = min_element(sum_weights.begin(), sum_weights.end()) - sum_weights.begin();
	cout<<"Directed MPkIS Construction Completed! Size is: "<<MCKIS[it].size()<<endl;
	/* Returned edge subset is the final solution i.e. the Desired MPkIS spanning subgraph */
	return MCKIS[it];
}

#endif