#ifndef MATROID_M2_INCLUDED
#define MATROID_M2_INCLUDED

#include <iostream>
#include <deque>
#include <vector>
#include <set>
#include <string>
#include <algorithm>
#include <math.h>
#include <time.h>
#include <iomanip>
#include <boost/multiprecision/cpp_int.hpp>
#include "Graph_Def.h"

vector<vector<graph_traits<digraph>::edge_descriptor>> matroid_m2(digraph g_m2, graph_traits<digraph>::vertex_descriptor root, int connectivity)
{
	
   	/********************************************************* Matroid M2 Construction **********************************************************************/

	/* Create a DOT format file to generate a visual representation of the graph */
	string output2 = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\MPkCS\\Graph_M2.dot";
	ofstream dot2(output2);

	/* Represent graph in DOT format and send to output file with a .dot extension */
    write_graphviz(dot2, g_m2);
    dot2.close();
	
	/* Create a shell command to execute the dot.exe program that generates a graphics version of a DOT format graph */
	string command2 = "C:\\Users\\ggoel\\graphviz-2.36\\release\\bin\\dot -Kneato -Tjpg ";
	command2.append(output2);
	command2.append(" -O");

	/* Execute the shell command */
	FILE *dot_output2;
	dot_output2 = _popen(command2.c_str(), "rt");
	_pclose(dot_output2);

	
	/** The Ground set of this matroid is all edges of the input graph (including all edges with root node as the tail). However, the ground set can further be reduced
	by the following observation of Andras Frank (which is given in Harold Gabow [1993]): the Final solution to the rooted k-connected from s subgraph, with minimal
	number of edges has in-degree of each vertex [belonging to all base vertices minus the root vertex] equal to k (the connecitivty requirement). Also Frank [2009]
	mentions that a subset of edges is independent in Matroid M2 if the indegree of the root vertex = 0. However, We do not know the out-degree constraints on the 
	vertices. Now constructing the ground set of the Matroid M2 **/
	
	/* Finding only those subsets of the edges of the base graph that have cardinality = connectvity*(num_nodes - 1) */
	graph_traits<digraph>::edge_iterator ei_m2, ei_end_m2;
	unsigned cardinality = connectivity * (num_vertices(g_m2) - 1);
	int num_edges_m2 = 0;
	num_edges_m2 = boost::num_edges(g_m2);
	
	cpp_int s1 = (cpp_int(1) << cardinality) - 1;
	vector<cpp_int> edge_subset;
	//std::deque<cpp_int> edge_subset;
	//int count = 0;
	while (!(s1 & cpp_int(1) << num_edges_m2))
	{
		//count++;
		//cout<<count<<endl;
		edge_subset.push_back(s1);
	    cpp_int lo = s1 & ~(s1 - 1);       // lowest one bit
	    cpp_int lz = (s1 + lo) & ~s1;      // lowest zero bit above lo
        s1 |= lz;                     // add lz to the set
        s1 &= ~(lz - 1);              // reset bits below lz
        s1 |= (lz / lo / 2) - 1;      // put back right number of bits at end
    }
	std::cout<<"Total possible combinations are: "<<edge_subset.size()<<"\n";

	typedef graph_traits<digraph>::edge_descriptor ed_m2;
	vector<vector<ed_m2>> edge_subsets_m2(edge_subset.size());
	//cpp_int** index_m2;
	bool** index_m2;
	//index_m2 = new cpp_int*[edge_subsets_m2.size()];
	index_m2 = new bool*[edge_subsets_m2.size()];
	cout<<"Test0"<<endl;
	for (int i = 0; i < edge_subsets_m2.size();  i++)
	{
		//index_m2[i].push_back(vector<int>(num_edges_m2));
		//cout<<(i)<<endl;
		//index_m2[i] = new cpp_int[num_edges_m2];
		index_m2[i] = new bool[num_edges_m2];
	}
	cout<<"Test1"<<endl;
	cpp_int num_m2 = 0;
	/* Now represent the contents of edge_subset vector in binary and store in the 2D array index_m2 */
	for (size_t i = 0; i < edge_subsets_m2.size(); i++)
	{
		num_m2 = edge_subset[i];
		int j = 1;
		while(num_m2 >= 1)
		{
			cpp_int temp = num_m2 % 2;
			//index_m2[i][num_edges_m2 - j] = temp;
			if (temp == 1)
				index_m2[i][num_edges_m2 - j] = true;
			else
				index_m2[i][num_edges_m2 - j] = false;
			num_m2 = num_m2 / 2;
			j++;
		}
	}
	cout<<"Test2"<<endl;
	/* Entering all possible subsets of edges in edge_subsets_m2 that have cardinality = connectivity * (num_vertices - 1) */
	for(size_t i = 0; i < edge_subsets_m2.size(); i++)
	{
		int j = 0;
		boost::tie(ei_m2, ei_end_m2) = edges(g_m2);
		while((j < num_edges_m2) && (ei_m2 != ei_end_m2))
		{
			//if(index_m2[i][j] == 1)
			if(index_m2[i][j])
			{
					edge_subsets_m2[i].push_back(*ei_m2);
			}
			j++;
			ei_m2++;
		}
	}
	delete [] index_m2;
	cout<<"Test3"<<endl;
	/** Now Creating the Matroid M2 based on the Independence Oracle for M2 **/
	vector<vector<ed_m2>> independent_m2;
	graph_traits<digraph>::in_edge_iterator in_ei_m2, in_ei_end_m2;
	graph_traits<digraph>::adjacency_iterator ai_m2, ai_end_m2;
	graph_traits<digraph>::vertex_iterator vi_m2, vi_end_m2;
	typedef graph_traits<digraph>::vertex_descriptor vd_m2;
	graph_traits<digraph>::in_edge_iterator in_ei, in_ei_end;
    vd_m2 source1, source2, target1, target2;
	ed_m2 ed_adjascent_m2;
	int sum;
	cout<<"Test Hello! "<<endl;
	for (size_t i = 0; i < edge_subsets_m2.size(); i++)
	{
		int vertex_count = 0;
		for(boost::tie(vi_m2, vi_end_m2) = vertices(g_m2); vi_m2 != vi_end_m2; vi_m2++)
		{	if ((*vi_m2) != root){
			/* No need to Check for the condition whether the indegree of the root vertex as induced by the subset is 0 or not. If not, it skips the rest of the iterations through the 
			vertices without adding the subset to the set of independent subsets of the matroid */
			vector<ed_m2> in_edges_m2;
			for (boost::tie(in_ei, in_ei_end) = in_edges((*vi_m2), g_m2); in_ei != in_ei_end; in_ei++)
			{
				in_edges_m2.push_back(*in_ei);
			}
			sum = 0;
			for (size_t j = 0; j < in_edges_m2.size(); j++)
			{
				source1 = source (in_edges_m2[j], g_m2);
				target1 = target (in_edges_m2[j], g_m2);
				for (size_t k = 0; k < edge_subsets_m2[i].size(); k++)
				{
					source2 = source (edge_subsets_m2[i][k], g_m2);
					target2 = target (edge_subsets_m2[i][k], g_m2);
					if ((source1 == source2) && (target1 == target2))
					{
						sum++;
						break;
					}
				}
			}
			if (sum <= connectivity)
				vertex_count++;
		}
		if ((vertex_count != (num_vertices(g_m2) - 1)))
			continue;
		else
			independent_m2.push_back(edge_subsets_m2[i]);
		}}
	cout<<"M2 Construction Completed! Size is: "<<independent_m2.size()<<endl;
	/********************************************************* Matroid M2 Construction Ends **********************************************************************/
	return independent_m2;
}
#endif