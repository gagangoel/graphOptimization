/* For All-to-One routing scheme: (1) Compute the max-degree-vertex in the graph (2) Assign it as the sink vertex (3) Compute undirected MPkCS. Also note that 
there can be instances of graph wich do not result in a feasible solution to Restricted MPkIS, in which case only the undirected MPkIS is possible
Following changes are required to implement the MPkCS in as less time as possible:
(1) Restrict cardinality of Free Matroid to K
(2) Set lower bound for cardinality of Master Matroids to K(|v| - 1) - K
(3) While forming set of all possible Restricted MPkIS, in the inner loop, remove the condition which tests for whether the given vertex is max-degree-vertex or not,
as all vertices are to be considered while forming MPkCS
*/

#include "Undirected_MPkIS.h"
#include "MPkCS.h"
#include "vertex_connectivity.hpp"

/* Set Received power threshold (in dBm) for node connections */
#define RECEIVER_SENSITIVITY -80.00

void MPkCS(vector<vector<double>> path_atten, vector<string> node_names, vector<site_struct> site_list, istream& in, string model)
{
	// Computing number of CPU clocks consumed to calculate the undirected MPkIS Spanning Subgraph
	clock_t t;
	t = clock();
	
	/* Number of nodes/vertices in the network */
	int num_nodes = node_names.size();

	unsigned connectivity = 2;
	undigraph un_g;
	graph_traits<undigraph>::vertex_descriptor v;
	graph_traits<undigraph>::vertex_descriptor root;
	bool inserted;

	property_map<undigraph, vertex_index_t>::type index_map = get(vertex_index, un_g);
	property_map<undigraph, vertex_name_t>::type name_map = get(vertex_name, un_g);
	property_map<undigraph, vertex_attribute_t>::type attr_map = get(vertex_attribute, un_g);
	property_map<undigraph, vertex_mp_width_t>::type width_map = get(vertex_mp_width, un_g);
	property_map<undigraph, vertex_mp_shape_t>::type shape_map = get(vertex_mp_shape, un_g);
	property_map<undigraph, vertex_mp_fixedsize_t>::type fixedsize_map = get(vertex_mp_fixedsize, un_g);
	property_map<undigraph, vertex_mp_mfontsize_t>::type vfontsize_map = get(vertex_mp_mfontsize, un_g);
	property_map<undigraph, vertex_mp_pos_t>::type pos_map = get(vertex_mp_pos, un_g);
	property_map<undigraph, edge_name_t>::type edge_name_map = get(edge_name, un_g);
	property_map<undigraph, edge_weight_t>::type weight_map = get(edge_weight, un_g);
	property_map<undigraph, edge_mp_fontsize_t>::type fontsize_map = get(edge_mp_fontsize, un_g);
	property_map<undigraph, edge_mp_ecolor_t>::type color_map = get(edge_mp_ecolor, un_g);
	property_map<undigraph, edge_mp_style_t>::type style_map = get(edge_mp_style, un_g);

	dynamic_properties dp;
	dp.property("index", index_map);
	dp.property("label", name_map);
	dp.property("width", width_map);
	dp.property("shape", shape_map);
	dp.property("fixedsize", fixedsize_map);
	dp.property("fontsize", vfontsize_map);
	dp.property("pos", pos_map);
	dp.property("label", edge_name_map);
	dp.property("weight", weight_map);
	dp.property("fontsize", fontsize_map);
	dp.property("color", color_map);
	dp.property("style", style_map);

	ostringstream out;

	/* Loop through all network nodes, adding vertices to the graph object. Set graph vertex names to node names, also assigning other node attributes */
	for(int i=0; i < num_nodes; i++)
		{	
			out.str(string());
			v = add_vertex(un_g);
			name_map[v] = node_names[i];
			out<<(site_list[i].X_pos*20.0)<<","<<(site_list[i].Y_pos*20.0);
			pos_map[v] = out.str();
			shape_map[v] = "circle";
			width_map[v] = 0.51;
			vfontsize_map[v] = 8.0;
			fixedsize_map[v] = "true";
			attr_map[v]["label"] = node_names[i];
		}

	/* Parse Vertex_Edge_Connectivity_* input file to find minimum transmission power level that ensures full network k-Connectivity (i.e. has vertex connectivity = k) */
	string temp_line;

	/* Discard labels (i.e. First line of input file) */
    getline(in, temp_line);

	/* Parse input file considering all border cases */
    vector <double> power, v_conn, e_conn, v_conn_local;
    double temp;

    while(getline(in, temp_line))
    {
        stringstream linestream(temp_line);
        linestream>>temp;
        power.push_back(temp);
        linestream>>temp;
        v_conn.push_back(temp);
        linestream>>temp;
        e_conn.push_back(temp);
		linestream>>temp;
		linestream>>temp;
		v_conn_local.push_back(temp);
    }

    int power_levels = power.size();
    cout << "Number of Power Levels: " <<power_levels<<endl;

	/* Now finding Minimum Power level ensuring given requirement of connectivity */
    double min_power;
	
    /*if ((v_conn[0] < connectivity)) // If maximum power level does not ensure "connectivity" connectivity, then MPkCS is not possible
	{
		cout<<"Min-Power k Connected Spanning Subgraph is not possible!"<<endl;
		return;
	}
    else if (v_conn[power_levels - 1] >= connectivity)
        min_power = power[power_levels - 1];
    else
    {
        for(unsigned int i = 0; i < power.size() - 1; i++)
        {
            if ((v_conn[i] >= connectivity) && (v_conn[i+1]) <= connectivity - 1)
            {
                min_power = power[i];
                break;
            }

        }
    }*/

	if ((v_conn_local[0] < connectivity)) // If maximum power level does not ensure "connectivity" connectivity, then MPkIS to Sink is not possible
	{
		cout<<"Min-Power k Inconnected to the Sink graph is not possible!"<<endl;
		return;
	}
    else if (v_conn_local[power_levels - 1] >= connectivity)
        min_power = power[power_levels - 1];
    else
    {
        for(unsigned int i = 0; i < power.size() - 1; i++)
        {
            if ((v_conn_local[i] >= connectivity) && (v_conn_local[i+1]) <= connectivity - 1)
            {
                min_power = power[i];
                break;
            }

        }
    }

    cout<<"Minimum Power ensuring (atleast) "<<connectivity<<"-connected network: "<<min_power<<endl;
	
	graph_traits<undigraph>::vertex_iterator vi, vi_end, vi_next;
	typedef graph_traits<undigraph>::edge_descriptor Edge;
	double rx_power_forward, rx_power_reverse;
	
	/* Re-build graph for minimum power level ensuring network k-connectivity */
	/* Traverse Path Attenuation matrix and add edges only if the path loss is less than threshold (Since negative path losses are used, path loss greater than threshold means link between the two nodes is possible) */
	Edge e;
	out.str(string());
	ostringstream out2;
	out2.setf(ios::fixed);
	out2.setf(ios::showpoint);
	out2.precision(2);

	for(boost::tie(vi, vi_end) = vertices(un_g); vi!=vi_end; vi++)
		{
			for(vi_next = vi+1; vi_next!=vi_end; vi_next++)
				{	
					rx_power_forward = min_power + path_atten[*vi][*vi_next];
					rx_power_reverse = min_power + path_atten[*vi_next][*vi];
					/* Undirected graph is assummed. Assign edges only if nodeA -> nodeB path loss AND nodeB -> nodeA path loss exceed threshold */
					if((rx_power_forward >= RECEIVER_SENSITIVITY) && (rx_power_reverse >= RECEIVER_SENSITIVITY))
					{
						out2.str(string());
						boost::tie(e, inserted) = add_edge((*vi), (*vi_next), un_g);
						weight_map[e] = (-1.0 * path_atten[*vi][*vi_next]);
						out2<<(-1.0 * path_atten[*vi][*vi_next]);
						edge_name_map[e] = out2.str();
						fontsize_map[e] = 12.0;
						color_map[e] = "black";
						style_map[e] = "";
					}
				}
		}

	/* REDUNDANT: Iterating through all vertices of the input graph, considering each as the root vertex and storing the computed MPkIS subgraphs for each vertex in a 2D vector */
	/* Since "All-to-One routing scheme is considered, assign as sink, the maximum degree vertex in the graph and all other vertices will be considered as source vertices" */
	int max_vertex_degree = 0;
	graph_traits<undigraph>::vertex_descriptor max_degree_vertex;
	for (boost::tie(vi, vi_end) = vertices(un_g); vi != vi_end; vi++)
	{
		if (max_vertex_degree < out_degree(*vi, un_g))
		{
			max_vertex_degree = out_degree(*vi, un_g);
			max_degree_vertex = *vi;
		}
	}
	//max_degree_vertex = graph_traits<undigraph>::vertex_descriptor(5);
	cout<<"Maximum degree vertex is: "<<max_degree_vertex<<endl;
	max_degree_vertex = graph_traits<undigraph>::vertex_descriptor(0);
	cout<<"New Maximum degree vertex is: "<<max_degree_vertex<<endl;
	cout<<"Degree of this vertex: "<<int(degree(max_degree_vertex, un_g))<<endl;
	//return;
	vector<vector<graph_traits<undigraph>::edge_descriptor>> Restricted_MPkIS;
	for (boost::tie(vi, vi_end) = vertices(un_g); vi != vi_end; vi++)
	{
		root = *vi;
		if (root == max_degree_vertex)
		{
		/* Now finding undirected MPkIS for a given root vertex */
		vector<graph_traits<undigraph>::edge_descriptor> temp_Restricted_MPkIS = _Undirected_MPkIS(un_g, root, connectivity);
		if (int(temp_Restricted_MPkIS.size()) == 0)
			continue;
		else
			Restricted_MPkIS.push_back(temp_Restricted_MPkIS);
		}
	}

	ofstream fout("Restricted_MPkIS.txt");
	for (size_t i = 0; i < Restricted_MPkIS.size(); i++)
	{
		for (size_t j = 0; j < Restricted_MPkIS[i].size(); j++)
			fout<<Restricted_MPkIS[i][j]<<" ";
		fout<<endl;
	}
	fout.close();

	graph_traits<undigraph>::vertex_descriptor source_undirected, target_undirected;
	graph_traits<undigraph>::edge_descriptor ed;
	int index;
	vector<double> power_subgraphs;
	/* Procedure to compute Power of the Minimum Power Graph Computed */
	for (size_t i = 0; i < Restricted_MPkIS.size(); i++)
	{
		//vector<vector<graph_traits<undigraph>::edge_descriptor>> vertex_edges_temp;
		vector<vector<double>> vertex_edges_temp;
		index = 0;
		for (boost::tie(vi, vi_end) = vertices(un_g); vi != vi_end; vi++)
		{
			//vertex_edges_temp.push_back(vector<graph_traits<undigraph>::edge_descriptor>());
			vertex_edges_temp.push_back(vector<double>());
			for (size_t j = 0; j < Restricted_MPkIS[i].size(); j++)
			{
				source_undirected = source(Restricted_MPkIS[i][j], un_g);
				target_undirected = target(Restricted_MPkIS[i][j], un_g);
				boost::tie(ed, inserted) = edge (source_undirected, target_undirected, un_g);
				if (inserted)
				{
					if (((*vi) == source_undirected) || ((*vi) == target_undirected))
					{
						//vertex_edges_temp[index].push_back(ed);
						vertex_edges_temp[index].push_back(weight_map[ed]);
					}
				}
			}
			index++;
		}
		vector<double> power_graph;
		for (size_t k = 0; k < vertex_edges_temp.size(); k++)
		{
			double power_vertex = *std::max_element(vertex_edges_temp[k].begin(), vertex_edges_temp[k].end());
			power_graph.push_back(power_vertex);
		}
		double sum_temp = 0.0;
		for(std::vector<double>::iterator j = power_graph.begin(); j != power_graph.end(); ++j)
			sum_temp += *j;
		power_subgraphs.push_back(sum_temp);
	}

	int min_graph_index = min_element(power_subgraphs.begin(), power_subgraphs.end()) - power_subgraphs.begin();
	for (size_t i = 0; i < power_subgraphs.size(); i++)
		cout<<"Power of the Computed Spanning Subgraph: "<<power_subgraphs[i]<<endl;

	/* Generating a dummy graph to compute vertex connectivity of the generated MPkCS (for the validation of connectivity requirements between the base graph and optimized graph) */
	undigraph graph_connectivity;
	for (size_t i = 0; i < Restricted_MPkIS[min_graph_index].size(); i++)
	{
		source_undirected = source(Restricted_MPkIS[min_graph_index][i], un_g);
		target_undirected = target(Restricted_MPkIS[min_graph_index][i], un_g);
		boost::tie(ed, inserted) = edge (source_undirected, target_undirected, un_g);
		if (inserted)
		{
			add_edge(source_undirected, target_undirected, graph_connectivity);
			color_map[ed] = "red";
			style_map[ed] = "bold";
		}
	}

	string output = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\Results\\Graph_MPkCS_";
	output.append(model);
	out.str(string());
	out<<min_power;
	output.append(out.str());
	output.append(".dot");
	ofstream dot(output);
    /* represent graph in DOT format and send to output file with a .dot extension */
    write_graphviz_dp(dot, un_g, dp, string("index"));
	//write_graphviz(dot, g);
    dot.close();
	/* Create a shell command to execute the dot.exe program that generates a graphics version of a DOT format graph */
	string command = "C:\\Users\\ggoel\\graphviz-2.36\\release\\bin\\dot -Kneato -n2 -Tjpg ";
	command.append(output);
	command.append(" -O");
	/* Execute the shell command */
	FILE *dot_output;
	dot_output = _popen(command.c_str(), "rt");
	_pclose(dot_output);
	
	/* Finding Vertex Connectivity of the Generated Graph */
	vector<graph_traits<undigraph>::vertex_descriptor> vertex_disconnecting_set;
	/* vertex_connectivity() function returns the minimum number of vertices needed to disconnect the network */
	int d = vertex_connectivity(graph_connectivity, back_inserter(vertex_disconnecting_set));
	cout<<"Connectivity of Computed Spanning Subgraph is: "<<d<<endl;

   t = clock() - t;
   cout<<"Time consumed: "<<t<<" clocks, "<<(double(t)/CLOCKS_PER_SEC)<<" seconds"<<endl;
}