/* Program to form a graphical representation of the network and compute edge and vertex connectivity */

#include "graph.h"
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/edge_connectivity.hpp>
#include "vertex_connectivity.hpp"
#include "vertex_connectivity_to_sink.hpp"

/* Set Received power threshold (in dBm) for node connections */
#define RECEIVER_SENSITIVITY -80.00

namespace boost{
/* Assigning custom node properties for better visualization of graph formed by Graphviz */
enum vertex_pos_t {vertex_pos};
enum vertex_shape_t {vertex_shape};
enum vertex_width_t {vertex_width};
enum vertex_fontsize_t (vertex_fontsize);
enum vertex_fixedsize_t (vertex_fixedsize);

BOOST_INSTALL_PROPERTY(vertex, pos);
BOOST_INSTALL_PROPERTY(vertex, shape);
BOOST_INSTALL_PROPERTY(vertex, width);
BOOST_INSTALL_PROPERTY(vertex, fontsize);
BOOST_INSTALL_PROPERTY(vertex, fixedsize);
}

using namespace std;
using namespace boost;

typedef std::map<std::string, std::string> vertexattr;

/* Declare the graph type for the network */
typedef property<vertex_name_t, string, property<vertex_pos_t, string, property<vertex_index_t, int, property<vertex_shape_t, string, property<vertex_attribute_t, vertexattr, property<vertex_width_t, double, property<vertex_fontsize_t, double, property<vertex_fixedsize_t, string>>>>>>>> v_prop;
typedef adjacency_list<vecS, vecS, undirectedS, v_prop> Graph;

void network_to_graph(vector<vector<double>> path_atten, vector<string> node_names, vector<site_struct> site_list, double tx_power, ostream& out2, string model)	
{
//	FIN (network_to_graph(path_atten, node_names, site_list, tx_power, out2, model))

	ostringstream convert;
	convert<<tx_power;
	
	/* Create a DOT format file to later generate a visual representation of the graph */
	string output = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\Results\\Graph_";
	output.append(model);
	output.append(convert.str());
	output.append(".dot");
	ofstream dot(output);
	
	/* Create a shell command to execute the dot.exe program that generates a graphics version of a DOT format graph */
	string command = "C:\\Users\\ggoel\\graphviz-2.36\\release\\bin\\dot -Kneato -n2 -Tjpg ";
	command.append(output);
	command.append(" -O");

	/* Number of nodes/vertices in the network */
	int num_nodes = node_names.size();
	
	/* Create an undirected graph object with the number of vertices equal to number of nodes in the network */
	Graph g;
	
	graph_traits<Graph>::vertex_descriptor v;
	
	property_map<Graph, vertex_name_t>::type name_map = get(vertex_name, g);
	property_map<Graph, vertex_index_t>::type index_map = get(vertex_index, g);
	property_map<Graph, vertex_attribute_t>::type attr_map = get(vertex_attribute, g);
	property_map<Graph, vertex_shape_t>::type shape_map = get(vertex_shape, g);
	property_map<Graph, vertex_pos_t>::type pos_map = get(vertex_pos, g);
	property_map<Graph, vertex_width_t>::type width_map = get(vertex_width, g);
	property_map<Graph, vertex_fontsize_t>::type fontsize_map = get(vertex_fontsize, g);
	property_map<Graph, vertex_fixedsize_t>::type fixedsize_map = get(vertex_fixedsize, g);
	
	dynamic_properties dp;
	dp.property("label", name_map);
	dp.property("index", index_map);
	dp.property("shape", shape_map);
	dp.property("pos", pos_map);
	dp.property("width", width_map);
	dp.property("fontsize", fontsize_map);
	dp.property("fixedsize", fixedsize_map);

	ostringstream out;
	
	/* Loop through all network nodes, adding vertices to the graph object. Set graph vertex names to node names, also assigning other node attributes */
	for(int i=0; i < num_nodes; i++)
		{	
			out.str(string());
			v = add_vertex(g);
			name_map[v] = node_names[i];
			out<<(site_list[i].X_pos*20.0)<<","<<(site_list[i].Y_pos*20.0);
			pos_map[v] = out.str();
			shape_map[v] = "circle";
			width_map[v] = 0.51;
			fontsize_map[v] = 8.0;
			fixedsize_map[v] = "true";
			attr_map[v]["label"] = node_names[i];
		}
	
	graph_traits<Graph>::vertex_iterator vi, vi_end, vi_next;
	double rx_power_forward, rx_power_reverse;
	
	/* Traverse Path Attenuation matrix and add edges only if the path loss is less than threshold (Since negative path losses are used, path loss greater than threshold means link between the two nodes is possible) */
	for(boost::tie(vi, vi_end) = vertices(g); vi!=vi_end; vi++)
		{
			for(vi_next = vi+1; vi_next!=vi_end; vi_next++)
				{	
					rx_power_forward = tx_power + path_atten[*vi][*vi_next];
					rx_power_reverse = tx_power + path_atten[*vi_next][*vi];
					/* Undirected graph is assummed. Assign edges only if nodeA -> nodeB path loss AND nodeB -> nodeA path loss exceed threshold
					if(path_atten[*vi][*vi_next] >= RECEIVER_SENSITIVITY && path_atten[*vi_next][*vi] >= RECEIVER_SENSITIVITY)*/
					if((rx_power_forward >= RECEIVER_SENSITIVITY) && (rx_power_reverse >= RECEIVER_SENSITIVITY))
					add_edge((*vi), (*vi_next), g);
				}
		}
	
	/* represent graph in DOT format and send to dot */
	write_graphviz_dp(dot, g, dp, string("index"));
	
	dot.close();
	
	/* Execute the shell command */
	FILE *dot_output;
	dot_output = _popen(command.c_str(), "rt");
	_pclose(dot_output);
	
	typedef graph_traits<Graph>::degree_size_type degree_size_type;
	
	/* Finding Edge Connectivity of the graph */
	
	typedef graph_traits<Graph>::edge_descriptor edge_descriptor;
	vector<edge_descriptor> disconnecting_set;
	
	/* edge_connectivity() function returns the minimum number of edges needed to disconnect the network */
	
	degree_size_type c = edge_connectivity(g, back_inserter(disconnecting_set));
	/* string edge_conn_output = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\Results\\Edge_Connectivity_";
	edge_conn_output.append(model);
	edge_conn_output.append(convert.str());
	edge_conn_output.append(".txt");
	ofstream connectivity(edge_conn_output);
	connectivity<<"Edge connectivity is: "<<c<<"."<<endl;
	connectivity<<"The disconnecting set is {";
	for(vector<edge_descriptor>::iterator i = disconnecting_set.begin(); i!= disconnecting_set.end(); ++i)
			connectivity<<"("<<attr_map[source(*i, g)]["label"]<<","<<attr_map[target(*i, g)]["label"]<<")";
	connectivity<<"}"<<endl;
	connectivity.close(); */
	
	/* Finding Vertex Connectivity of the graph */
	
	typedef graph_traits<Graph>::vertex_descriptor vertex_descriptor;
	vector<vertex_descriptor> vertex_disconnecting_set;
	vector<vertex_descriptor> vertex_disconnecting_set_local;
	
	/* vertex_connectivity() function returns the minimum number of vertices needed to disconnect the network */
	degree_size_type d = vertex_connectivity(g, back_inserter(vertex_disconnecting_set));

	/* vertex_connectivity_to_sink() function returns local vertex connectivity of every other vertex from sink (fixed vertex with id = 0) vertex */
	degree_size_type d_local = vertex_connectivity_to_sink(g, back_inserter(vertex_disconnecting_set_local));
	
	/* string vertex_conn_output = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\Results\\Vertex_Connectivity_";
	vertex_conn_output.append(model);
	vertex_conn_output.append(convert.str());
	vertex_conn_output.append(".txt");
	ofstream v_connectivity(vertex_conn_output);
	v_connectivity<<"Vertex connectivity is: ";
	v_connectivity<<d<<endl; */
	/*v_connectivity<<"The disconnecting set is {";
	for(vector<vertex_descriptor>::iterator j = vertex_disconnecting_set.begin(); j!= vertex_disconnecting_set.end(); ++j)
			v_connectivity<<"("<<attr_map[*j]["label"]<<") ";
	v_connectivity<<"}"<<endl;*/
	/*v_connectivity.close();*/
	
	int num_edges = boost::num_edges(g);
	
	string temp = convert.str();
	if (temp.length() == 1)
		out2<<temp<<"  ";
	else if(temp.length() == 2)
		out2<<temp<<" ";
	else
		out2<<temp;
	out2<<"				"<<d<<"				"<<c<<"		"<<num_edges<<"		"<<d_local<<endl;
//	FOUT
}
