#ifndef K_G_FOLIAGE_INCLUDED
#define K_G_FOLIAGE_INCLUDED

#include "Matroid_Master.h"
#include "Matroid_M2.h"
#include "Matroid_Free.h"
#include "Graph_Def.h"
#include <iterator>
#include <algorithm>
#include <vector>
#include <set>
#include <time.h>

using namespace std;
using namespace boost;

vector<vector<graph_traits<digraph>::edge_descriptor>> k_g_foliage(digraph g, graph_traits<digraph>::vertex_descriptor root, int connectivity)
{
   /* Removing all redundant edges before calling the Min-Cost Rooted k-Connected Algorithm */
   /* An edge subset is independent in Matroid M2 if the in-degreee of the root vertex induced by that subset is 0. Therefore removing all in-edges of root vertex 
   as they do not contribute to the final solution */
   clear_in_edges(root, g);

	/* Create a DOT format file to later generate a visual representation of the graph */
	string output = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\MPkCS\\k_g_foliage.dot";
	ofstream dot(output);
    /* represent graph in DOT format and send to output file with a .dot extension */
    //write_graphviz_dp(dot, g, dp, string("index"));
	write_graphviz(dot, g);
    dot.close();
	/* Create a shell command to execute the dot.exe program that generates a graphics version of a DOT format graph */
	string command = "C:\\Users\\ggoel\\graphviz-2.36\\release\\bin\\dot -Kneato -Tjpg ";
	command.append(output);
	command.append(" -O");
	/* Execute the shell command */
	FILE *dot_output;
	dot_output = _popen(command.c_str(), "rt");
	_pclose(dot_output);

	vector<vector<graph_traits<digraph>::edge_descriptor>> independent_free = free_matroid(g, root, connectivity);
	//cout<<"Total number of independent subsets of Free Matroid are: "<<independent_free.size()<<endl;
	vector<vector<graph_traits<digraph>::edge_descriptor>> independent_m2 =  matroid_m2(g, root, connectivity);
	//cout<<"Total number of independent subsets of Matroid M2 are: "<<independent_m2.size()<<endl;
	vector<vector<graph_traits<digraph>::edge_descriptor>> independent_master =  matroid_master(g, root, connectivity, independent_m2);
	//cout<<"Total number of independent subsets of Master Matroid are: "<<independent_master.size()<<endl;

	/* Computing Matroid M1: Direct Sum of Free and Master Matroids */
	int m1_size = (independent_free.size()) * (independent_master.size()); 
	vector<vector<graph_traits<digraph>::edge_descriptor>> independent_m1;

	int index = 0;
	for (size_t i = 0; i < independent_free.size(); i++)
	{
		if ((independent_free[i].size()) == (connectivity * (num_vertices(g) - 1)))
			{
				independent_m1.push_back(vector<graph_traits<digraph>::edge_descriptor>());
				independent_m1[index] = independent_free[i];
				index++;
			}
	}
	for (size_t i = 0; i < independent_master.size(); i++)
	{
		for (size_t j = 0; j < independent_free.size(); j++)
		{
			if ((independent_master[i].size() + independent_free[j].size()) == (connectivity * (num_vertices(g) - 1))) // Considering only those subsets of M1 which have cardinality = k * (num_vertices - 1)
			{
				independent_m1.push_back(vector<graph_traits<digraph>::edge_descriptor>());
				independent_m1[index] = independent_master[i];
				independent_m1[index].insert(independent_m1[index].end(), independent_free[j].begin(), independent_free[j].end());
				index++;
			}
		}
	}

	ofstream fout ("m1_subsets.txt");
	for (size_t i = 0; i < independent_m1.size(); i++)
	{
		for (size_t j = 0; j < independent_m1[i].size(); j++)
			fout<<independent_m1[i][j]<<" ";
		fout<<endl;
	}
	fout.close();

	ofstream fout2 ("m2_subsets.txt");
	for (size_t i = 0; i < independent_m2.size(); i++)
	{
		for (size_t j = 0; j < independent_m2[i].size(); j++)
			fout2<<independent_m2[i][j]<<" ";
		fout2<<endl;
	}
	fout2.close();

	//cout<<"Total number of Independent subsets of Matroid M1 are: "<<independent_m1.size()<<endl;

  /* Now finding Intersection of Matroids M1 and M2 */
  typedef graph_traits<digraph>::edge_descriptor ed;
  typedef graph_traits<digraph>::vertex_descriptor vd;
  vd s_m1, s_m2, t_m1, t_m2;
  vector<vector<ed>> k_g_foliage;
  int sum = 0;
  for (size_t i = 0; i < independent_m1.size(); i++)
  {
	  for (size_t j = 0; j < independent_m2.size(); j++)
	  {
		  sum = 0;
		  for (size_t k = 0; k < independent_m1[i].size(); k++)
		  {
			  s_m1 = source (independent_m1[i][k], g);
			  t_m1 = target (independent_m1[i][k], g);
			  for (size_t l = 0; l < independent_m2[j].size(); l++)
			  {
				  s_m2 = source (independent_m2[j][l], g);
				  t_m2 = target (independent_m2[j][l], g);
				  if ((s_m2 == s_m1) && (t_m2 == t_m1))
				  {
					  sum++;
					  break;
				  }
			  }
		  }
		  if (sum == int(independent_m1[i].size()))
		  {
			  k_g_foliage.push_back(independent_m1[i]);
			  break;
		  }
	  }
  }
	cout<<"K-G Foliage Construction Completed! Size is: "<<k_g_foliage.size()<<endl;
   return k_g_foliage;
}

#endif