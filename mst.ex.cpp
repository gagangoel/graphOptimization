/* Implementation of Minimum Spanning Tree algorithm for a 2-approximation to the Optimal Power Assignment Problem for a 1-connected Graph */
/* Min-Power 1-Connected Spanning Subgraph Problem */
/* The Time Complexity of the Kruskal's Minimum Spanning Tree algorithm is O(mlogm), m: number of edges in the graph (also called the size of graph) */

#include "mst.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/edge_connectivity.hpp>
#include "vertex_connectivity.hpp"

/* Set Received power threshold (in dBm) for node connections */
#define RECEIVER_SENSITIVITY -80.00

namespace boost
{
	/* Assigning custom node properties for better visualization of graph formed by Graphviz */
	enum vertex_pos_t {vertex_pos};
	enum vertex_shape_t {vertex_shape};
	enum vertex_width_t {vertex_width};
	enum vertex_fixedsize_t {vertex_fixedsize};
	enum vertex_mfontsize_t (vertex_mfontsize);
	enum edge_fontsize_t {edge_fontsize};
	enum edge_ecolor_t {edge_ecolor};
	enum edge_style_t {edge_style};

	BOOST_INSTALL_PROPERTY(vertex, pos);
	BOOST_INSTALL_PROPERTY(vertex, shape);
	BOOST_INSTALL_PROPERTY(vertex, width);
	BOOST_INSTALL_PROPERTY(vertex, fixedsize);
	BOOST_INSTALL_PROPERTY(vertex, mfontsize);
	BOOST_INSTALL_PROPERTY(edge, fontsize);
	BOOST_INSTALL_PROPERTY(edge, ecolor);
	BOOST_INSTALL_PROPERTY(edge, style);
}

using namespace boost;
using namespace std;

typedef std::map<std::string, std::string> vertexattr;

/* Declare the graph type for the network */
typedef property<vertex_index_t, int, property<vertex_name_t, string, property<vertex_width_t, double, property<vertex_shape_t, string, property<vertex_fixedsize_t, string, property<vertex_pos_t, string, property<vertex_mfontsize_t, double, property<vertex_attribute_t, vertexattr>>>>>>>> v_prop;
typedef property<edge_name_t, string, property<edge_weight_t, double, property<edge_fontsize_t, double, property<edge_ecolor_t, string, property<edge_style_t, string>>>>> e_prop;
typedef adjacency_list<vecS, vecS, undirectedS, v_prop, e_prop> Graph;

void mst(vector<vector<double>> path_atten, vector<string> node_names, vector<site_struct> site_list, istream& in, string model)
{
//	FIN (mst(path_atten, node_names, site_list, in, model))

	/* Number of nodes/vertices in the network */
	int num_nodes = node_names.size();

	/* Create an undirected graph object with the number of vertices equal to number of nodes in the network */
	Graph g;
	graph_traits<Graph>::vertex_descriptor v;
	
	property_map<Graph, vertex_index_t>::type index_map = get(vertex_index, g);
	property_map<Graph, vertex_name_t>::type name_map = get(vertex_name, g);
	property_map<Graph, vertex_attribute_t>::type attr_map = get(vertex_attribute, g);
	property_map<Graph, vertex_width_t>::type width_map = get(vertex_width, g);
	property_map<Graph, vertex_shape_t>::type shape_map = get(vertex_shape, g);
	property_map<Graph, vertex_fixedsize_t>::type fixedsize_map = get(vertex_fixedsize, g);
	property_map<Graph, vertex_mfontsize_t>::type vfontsize_map = get(vertex_mfontsize, g);
	property_map<Graph, vertex_pos_t>::type pos_map = get(vertex_pos, g);
	property_map<Graph, edge_name_t>::type edge_name_map = get(edge_name, g);
	property_map<Graph, edge_weight_t>::type weight_map = get(edge_weight, g);
	property_map<Graph, edge_fontsize_t>::type fontsize_map = get(edge_fontsize, g);
	property_map<Graph, edge_ecolor_t>::type color_map = get(edge_ecolor, g);
	property_map<Graph, edge_style_t>::type style_map = get(edge_style, g);

	dynamic_properties dp;
	dp.property("index", index_map);
	dp.property("label", name_map);
	dp.property("width", width_map);
	dp.property("shape", shape_map);
	dp.property("fixedsize", fixedsize_map);
	dp.property("fontsize", vfontsize_map);
	dp.property("pos", pos_map);
	dp.property("label", edge_name_map);
	dp.property("weight", weight_map);
	dp.property("fontsize", fontsize_map);
	dp.property("color", color_map);
	dp.property("style", style_map);

	ostringstream out;

	/* Loop through all network nodes, adding vertices to the graph object. Set graph vertex names to node names, also assigning other node attributes */
	for(int i=0; i < num_nodes; i++)
		{	
			out.str(string());
			v = add_vertex(g);
			name_map[v] = node_names[i];
			out<<(site_list[i].X_pos*20.0)<<","<<(site_list[i].Y_pos*20.0);
			pos_map[v] = out.str();
			shape_map[v] = "circle";
			width_map[v] = 0.51;
			vfontsize_map[v] = 8.0;
			fixedsize_map[v] = "true";
			attr_map[v]["label"] = node_names[i];
		}

	/* Parse Vertex_Edge_Connectivity_* input file to find minimum transmission power level that ensures full network connectivity (i.e. has vertex connectivity = 1) */
	string temp_line;

	/* Discard labels (i.e. First line of input file) */
    getline(in, temp_line);

	/* Parse input file considering all border cases */
    vector <double> power, v_conn, e_conn;
    double temp;
    while(getline(in, temp_line))
    {
        stringstream linestream(temp_line);
        linestream>>temp;
        power.push_back(temp);
        linestream>>temp;
        v_conn.push_back(temp);
        linestream>>temp;
        e_conn.push_back(temp);
    }

    int power_levels = power.size();
    cout << "Number of Power Levels: " <<power_levels<<endl;

    double min_power;
    if (v_conn[0] == 0)
        min_power = power[0];
    else if (v_conn[power_levels - 1] >= 1)
        min_power = power[power_levels - 1];
    else
    {
        for(unsigned int i = 0; i < power.size() - 1; i++)
        {
            if ((v_conn[i] != 0) && (v_conn[i+1]) == 0)
            {
                min_power = power[i];
                break;
            }

        }
    }

    cout<<"Minimum Power ensuring (atleast) 1-connected network: "<<min_power<<endl;

	graph_traits<Graph>::vertex_iterator vi, vi_end, vi_next;
	typedef graph_traits<Graph>::edge_descriptor Edge;
	double rx_power_forward, rx_power_reverse;

	/* Re-build graph for minimum power level ensuring network connectivity */
	/* Traverse Path Attenuation matrix and add edges only if the path loss is less than threshold (Since negative path losses are used, path loss greater than threshold means link between the two nodes is possible) */
	bool inserted;
	Edge e;
	out.str(string());
	ostringstream out2;
	out2.setf(ios::fixed);
	out2.setf(ios::showpoint);
	out2.precision(2);
	
	for(boost::tie(vi, vi_end) = vertices(g); vi!=vi_end; vi++)
		{
			for(vi_next = vi+1; vi_next!=vi_end; vi_next++)
				{	
					rx_power_forward = min_power + path_atten[*vi][*vi_next];
					rx_power_reverse = min_power + path_atten[*vi_next][*vi];
					/* Undirected graph is assummed. Assign edges only if nodeA -> nodeB path loss AND nodeB -> nodeA path loss exceed threshold */
					if((rx_power_forward >= RECEIVER_SENSITIVITY) && (rx_power_reverse >= RECEIVER_SENSITIVITY))
					{
						out2.str(string());
						boost::tie(e, inserted) = add_edge((*vi), (*vi_next), g);
						weight_map[e] = (-1.0 * path_atten[*vi][*vi_next]);
						out2<<(-1.0 * path_atten[*vi][*vi_next]);
						edge_name_map[e] = out2.str();
						fontsize_map[e] = 12.0;
						color_map[e] = "black";
						style_map[e] = "";
					}
				}
		}

	std::vector<Edge> mst;
	kruskal_minimum_spanning_tree(g, std::back_inserter(mst));

	for(int i = 0; i<mst.size(); i++)
	{
		color_map[mst[i]] = "red";
		style_map[mst[i]] = "bold";
	}

	string output = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\Results\\Graph_MST_";
	output.append(model);
	out.str(string());
	out<<min_power;
	output.append(out.str());
	output.append(".dot");
	ofstream dot(output);

	write_graphviz_dp(dot, g, dp, string("index"));

	dot.close();

	/* Create a shell command to execute the dot.exe program that generates a graphics version (.jpg/.png) of a DOT format graph */
	string command = "C:\\Users\\ggoel\\graphviz-2.36\\release\\bin\\dot -Kneato -n2 -Tjpg ";
	command.append(output);
	command.append(" -O");

	/* Execute Shell Command */
	FILE *dot_output;
	dot_output = _popen(command.c_str(), "rt");
	_pclose(dot_output);

}
