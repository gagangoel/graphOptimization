#ifndef UNDIRECTED_MPKIS_INCLUDED
#define UNDIRECTED_MPKIS_INCLUDED

#include "Directed_MPkIS.h"

using namespace std;
using namespace boost;

vector<graph_traits<undigraph>::edge_descriptor> _Undirected_MPkIS(undigraph un_g, graph_traits<undigraph>::vertex_descriptor root, int connectivity)
{
	/* Creating storage for final result */
	vector<graph_traits<undigraph>::edge_descriptor> Undirected_MPkIS;

	graph_traits<undigraph>::vertex_iterator vi, vi_end;
	for (boost::tie(vi, vi_end) = vertices(un_g); vi != vi_end; vi++)
		if (int(degree((*vi), un_g)) < connectivity)
		{
			cout<<"Undirected Min-Power k-Inconneced-to-root Spanning Subgraph is not possible! "<<endl;
			cout<<Undirected_MPkIS.size()<<endl;
			return Undirected_MPkIS;
		}

	/* Now creating a Bi-Direction of the undirected graph */
	digraph g;
	property_map<digraph, edge_weight_t>::type weight_map_directed = get(edge_weight, g);
	property_map<digraph, vertex_index_t>::type index_map_directed = get(vertex_index, g);
   	property_map<digraph, edge_name_t>::type edge_name_map_directed = get(edge_name, g);
	property_map<digraph, edge_mp_ecolor_t>::type color_map_directed = get(edge_mp_ecolor, g);
	property_map<digraph, edge_mp_style_t>::type style_map_directed = get(edge_mp_style, g);
	
   	property_map<undigraph, edge_name_t>::type edge_name_map = get(edge_name, un_g);
	property_map<undigraph, edge_weight_t>::type weight_map = get(edge_weight, un_g);
	property_map<undigraph, edge_mp_ecolor_t>::type color_map = get(edge_mp_ecolor, un_g);
	property_map<undigraph, edge_mp_style_t>::type style_map = get(edge_mp_style, un_g);

	dynamic_properties dp_directed;
	dp_directed.property("index", index_map_directed);
	dp_directed.property("label", edge_name_map_directed);
	dp_directed.property("weight", weight_map_directed);
	dp_directed.property("color", color_map_directed);
	dp_directed.property("style", style_map_directed);
	graph_traits<undigraph>::edge_iterator ei, ei_end;
	graph_traits<digraph>::edge_descriptor ed1, ed2;
	bool inserted1, inserted2;
	graph_traits<undigraph>::vertex_descriptor source_undirected, target_undirected;
	for (boost::tie(ei, ei_end) = edges(un_g); ei != ei_end; ei++)
	{
		source_undirected = source(*ei, un_g);
		target_undirected = target(*ei, un_g);
		boost::tie(ed1, inserted1) = add_edge(source_undirected, target_undirected, g);
		boost::tie(ed2, inserted2) = add_edge(target_undirected, source_undirected, g);
		weight_map_directed[ed1] = weight_map[(*ei)];
		weight_map_directed[ed2] = weight_map[(*ei)];
		edge_name_map_directed[ed1] = edge_name_map[(*ei)];
		edge_name_map_directed[ed2] = edge_name_map[(*ei)];
		color_map_directed[ed1] = color_map[(*ei)];
		color_map_directed[ed2] = color_map[(*ei)];
		style_map_directed[ed1] = style_map[(*ei)];
		style_map_directed[ed2] = style_map[(*ei)];

	}

	bool inserted;
	graph_traits<digraph>::edge_descriptor ed;
	typedef graph_traits<digraph>::vertex_descriptor vd;
	graph_traits<undigraph>::edge_descriptor ed_undirected;
	bool inserted_undirected;
	vd source1, target1;
	vector<graph_traits<digraph>::edge_descriptor> Directed_MPkIS = _Directed_MPkIS(g, root, connectivity);
	if (int(Directed_MPkIS.size()) == 0)
	{
		vector<graph_traits<undigraph>::edge_descriptor> empty_Undirected_MPkIS;
		return empty_Undirected_MPkIS;
	}
	
    for (size_t j = 0; j < Directed_MPkIS.size(); j++)
	{
		source1 = source(Directed_MPkIS[j], g);
		target1 = target(Directed_MPkIS[j], g);
		boost::tie(ed, inserted) = edge (source1, target1, g);
		if (inserted)
		{
			color_map_directed[ed] = "red";
			style_map_directed[ed] = "bold";
		}
		source_undirected = source(Directed_MPkIS[j], un_g);
		target_undirected = target(Directed_MPkIS[j], un_g);
		boost::tie(ed_undirected, inserted_undirected) = edge (source_undirected, target_undirected, un_g);
		if (inserted_undirected)
		{
			color_map[ed_undirected] = "red";
			style_map[ed_undirected] = "bold";
		}
	}

	/* Storing the edge set for MPkIS Subgraph for the root vertex "root" */
	for(boost::tie(ei, ei_end) = edges(un_g); ei != ei_end; ei++)
	{
		if (color_map[(*ei)] == "red")
			Undirected_MPkIS.push_back((*ei));
	}

	/*cout<<Undirected_MPkIS.size()<<endl;
	for (size_t i = 0; i < Undirected_MPkIS.size(); i++)
		cout<<Undirected_MPkIS[i]<<" ";
	cout<<endl;*/

	/* Create a DOT format file to generate a visual representation of the graph */
	string output_directed = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\MPkCS\\directed_temp.dot";
	ofstream dot_directed(output_directed);
    /* represent graph in DOT format and send to output file with a .dot extension */
    write_graphviz_dp(dot_directed, g, dp_directed, string("index"));
    dot_directed.close();
	/* Create a shell command to execute the dot.exe program that generates a graphics version of a DOT format graph */
	string command_directed = "C:\\Users\\ggoel\\graphviz-2.36\\release\\bin\\dot -Kneato -Tjpg ";
	command_directed.append(output_directed);
	command_directed.append(" -O");
	/* Execute the shell command */
	FILE *dot_output_directed;
	dot_output_directed = _popen(command_directed.c_str(), "rt");
	_pclose(dot_output_directed);

	cout<<"Undirected MPkIS Construction Completed! Size is: "<<Undirected_MPkIS.size()<<endl;
	return Undirected_MPkIS;
}

#endif