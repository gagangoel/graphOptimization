#ifndef MATROID_FREE_INCLUDED
#define MATROID_FREE_INCLUDED

#include <vector>
#include "Graph_Def.h"

vector<vector<graph_traits<digraph>::edge_descriptor>> free_matroid(digraph g, graph_traits<digraph>::vertex_descriptor root, int connectivity)
{

	/****************************************************** Free Matroid Construction Starts *****************************************************************/

	/* Computing Free Matroid on the out-edge set of root vertex */
   int num_out_edges = out_degree(root, g);
   /* Create a 0-1 matrix to store the indices of the elements that form all possible subsets of the given base out-edge set */
	/* Create a 2D dynamic array to store the indices and initialize it with 0 */
	int** index;
	index = new int*[(int(pow(double(2), num_out_edges)) - 1)];
	for (int i = 0; i < (int(pow(double(2), num_out_edges)) - 1);  i++)
	{
		index[i] = new int[num_out_edges];
	}

	for (int i = 0; i < (int(pow(double(2), num_out_edges)) - 1);  i++)
	{
		for(int j = 0; j < num_out_edges; j++)
		{
			index[i][j] = 0;
		}
	}

	/* Now represent 1 to (2^(num_out_edges) - 1) in binary and store in the 2D array */
	int num;
	for (int i = 1; i < (int(pow(double(2), num_out_edges)) - 0);  i++)
	{
		num = i;
		int j = 1;
		while(num >= 1)
		{
			int temp = num % 2;
			index[i-1][num_out_edges - j] = temp;
			num = num / 2;
			j++;
		}
	}

	vector<vector<graph_traits<digraph>::edge_descriptor>> independent_free((int(pow(double(2), num_out_edges)) - 1));
	graph_traits<digraph>::out_edge_iterator out_ei, out_ei_end;

	/* Entering all possible subsets of out-edge set in independent_free vector */
	for(int i = 0; i < (int(pow(double(2), num_out_edges)) - 1); i++)
	{
		int j = 0;
		boost::tie(out_ei, out_ei_end) = out_edges(root, g);
		while((j < num_out_edges) && (out_ei != out_ei_end))
		{
			if(index[i][j] == 1)
			{
				independent_free[i].push_back(*out_ei);
			}
			j++;
			out_ei++;
		}
	}
	delete [] index;

	/****************************************************** Free Matroid Construction Ends *****************************************************************/
	vector<vector<graph_traits<digraph>::edge_descriptor>> independent_free_new;
	for (size_t i = 0; i < independent_free.size(); i++)
	{
		if (int(independent_free[i].size()) == connectivity)
			independent_free_new.push_back(independent_free[i]);
	}
	cout<<"Free Matroid Construction Completed! Size is: "<<independent_free.size()<<endl;
	//return independent_free_new;
	return independent_free;
}
#endif