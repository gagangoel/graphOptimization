#ifndef MST_H_INCLUDED
#define MST_H_INCLUDED

#include <vector>
#include <string>
#include "graph.h"

#if defined (__cplusplus)
extern "C" 
{
#endif

/*struct site_struct;
{
	std::string Name;		// Site Name
	double X_pos;		// Site X position
	double Y_pos;		// Site Y position

	/* Constructor */
/*	site_struct()
	{
		Name = "";
		X_pos = 0.0;
		Y_pos = 0.0;
	}
};*/

void mst(std::vector<std::vector<double>>, std::vector<std::string>, std::vector<extern site_struct>, std::istream&, std::string);
	
#if defined (__cplusplus)
} /* end of 'extern "C" */
#endif


#endif // MST_H_INCLUDED
