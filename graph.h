#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED

#include <vector>
#include <string>

#if defined (__cplusplus)
extern "C" 
{
#endif

struct site_struct
{
	std::string Name;		// Site Name
	double X_pos;		// Site X position
	double Y_pos;		// Site Y position

	/* Constructor */
	site_struct()
	{
		Name = "";
		X_pos = 0.0;
		Y_pos = 0.0;
	}
};

void network_to_graph(std::vector<std::vector<double>>, std::vector<std::string>, std::vector<site_struct>, double, std::ostream&, std::string);
	
#if defined (__cplusplus)
} /* end of 'extern "C" */
#endif


#endif // GRAPH_H_INCLUDED
