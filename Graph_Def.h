#ifndef GRAPH_DEF_INCLUDED
#define GRAPH_DEF_INCLUDED

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>

namespace boost
{
	enum vertex_mp_pos_t {vertex_mp_pos};
	enum vertex_mp_shape_t {vertex_mp_shape};
	enum vertex_mp_width_t {vertex_mp_width};
	enum vertex_mp_fixedsize_t {vertex_mp_fixedsize};
	enum vertex_mp_mfontsize_t (vertex_mp_mfontsize);
	enum edge_mp_fontsize_t {edge_mp_fontsize};
	enum edge_mp_ecolor_t {edge_mp_ecolor};
	enum edge_mp_style_t {edge_mp_style};

	BOOST_INSTALL_PROPERTY(vertex, mp_pos);
	BOOST_INSTALL_PROPERTY(vertex, mp_shape);
	BOOST_INSTALL_PROPERTY(vertex, mp_width);
	BOOST_INSTALL_PROPERTY(vertex, mp_fixedsize);
	BOOST_INSTALL_PROPERTY(vertex, mp_mfontsize);
	BOOST_INSTALL_PROPERTY(edge, mp_fontsize);
	BOOST_INSTALL_PROPERTY(edge, mp_ecolor);
	BOOST_INSTALL_PROPERTY(edge, mp_style);
}


using namespace std;
using namespace boost;
using boost::multiprecision::cpp_int;

typedef property<vertex_index_t, int> v_prop;
typedef property<edge_name_t, string, property<edge_weight_t, double, property<edge_mp_fontsize_t, double, property<edge_mp_ecolor_t, string, property<edge_mp_style_t, string>>>>> e_prop;
typedef adjacency_list< vecS, vecS, bidirectionalS, v_prop, e_prop > digraph;

typedef std::map<std::string, std::string> vertexattr;
typedef property<vertex_index_t, int, property<vertex_name_t, string, property<vertex_mp_width_t, double, property<vertex_mp_shape_t, string, property<vertex_mp_fixedsize_t, string, property<vertex_mp_pos_t, string, property<vertex_mp_mfontsize_t, double, property<vertex_attribute_t, vertexattr>>>>>>>> v_prop_complete;
typedef adjacency_list<vecS, vecS, undirectedS, v_prop_complete, e_prop> undigraph;

#endif