/* Note: Parts of the implementation are adapted from the Boost Graph Libraries */
/* Program to find Vertex Connectivity of a graph generated out of the Sensor Network Topology using Boost Graph Libraries: Particularly using Edmond's Karp Max-Flow algorithm
The algorithm makes O(n-delta-1+delta(delta-1)/2) calls to the max flow algorithm. The max-flow algorithm has a time complexity O((n^(2/3))m). Here, n: order of the graph, m: size of the graph */

#ifndef VERTEX_CONNECTIVITY_H_INCLUDED
#define VERTEX_CONNECTIVITY_H_INCLUDED

#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <boost/config.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>

namespace boost
{
namespace detail2
	{
		template <class Graph>
		inline std::pair<typename graph_traits<Graph>::vertex_descriptor, typename graph_traits<Graph>::degree_size_type> min_degree_vertex(Graph &g)
			{
				typedef graph_traits<Graph> Traits;
				typename Traits::vertex_descriptor p;
				typedef typename Traits::degree_size_type size_type;
				size_type delta = (std::numeric_limits<size_type>::max)();

				typename Traits::vertex_iterator i, iend;
				for (boost::tie(i, iend) = vertices(g); i!= iend; i++)
					if(degree(*i, g) < delta)
						{
							delta = degree(*i, g);
							p = *i;
						}
				return std::make_pair(p, delta);
			}

		template <class Graph, class OutputIterator>
		void neighbors(const Graph &g, typename graph_traits<Graph>::vertex_descriptor u, OutputIterator result)
			{
				typename graph_traits<Graph>::adjacency_iterator ai, aend;
				for(boost::tie(ai, aend) = adjacent_vertices(u, g); ai!=aend; ai++)
					*result++ = *ai;
			}

		template <class Graph, class VertexIterator, class OutputIterator>
		void neighbors(const Graph &g, VertexIterator first, VertexIterator last, OutputIterator result)
			{
				for(; first!=last; first++)
					{
						neighbors(g, *first, result);
					}
			}
	} /* namespace detail2 */

template <class VertexListGraph, class OutputIterator>
typename graph_traits<VertexListGraph>::degree_size_type vertex_connectivity (VertexListGraph &g, OutputIterator vertex_disconnecting_set)
{
    /* Type Definitions */
    typedef graph_traits<VertexListGraph> Traits;
    typedef typename Traits::vertex_iterator vertex_iterator;
    typedef typename Traits::out_edge_iterator out_edge_iterator;
    typedef typename Traits::vertex_descriptor vertex_descriptor;
    typedef typename Traits::degree_size_type degree_size_type;
    typedef typename Traits::edge_iterator edge_iterator;
    typedef adjacency_list_traits<vecS, vecS, directedS> Tr;
    typedef Tr::edge_descriptor Tr_edge_desc;
    typedef adjacency_list<vecS, vecS, directedS, no_property, property<edge_capacity_t, degree_size_type, property<edge_residual_capacity_t, degree_size_type, property<edge_reverse_t, Tr_edge_desc> > > > FlowGraph;
    typedef typename graph_traits<FlowGraph>::edge_descriptor flow_graph_edge_descriptor;
    typedef typename graph_traits<FlowGraph>::vertex_descriptor flow_graph_vertex_descriptor;
    typedef typename graph_traits<FlowGraph>::out_edge_iterator flow_graph_edge_iterator;
    typedef typename graph_traits<FlowGraph>::vertex_iterator flow_graph_vertex_iterator;
    typedef typename graph_traits<FlowGraph>::edge_iterator flow_graph_edge_iter;

    /* Variable declaration */
    typename graph_traits<VertexListGraph>::edge_descriptor g_e;
    vertex_descriptor p, k, x, y, u, v;
    vertex_iterator vi, vi_end;
    degree_size_type delta, alpha_star, alpha_S_k;
    std::vector<degree_size_type> out_degree;
    std::set<vertex_descriptor> S, neighbor_S;
    typename std::set<vertex_descriptor>::iterator iter_x, iter_y;
    std::vector<vertex_descriptor> non_neighbor_S;

    FlowGraph flow_g(num_vertices(g));

    typename property_map<FlowGraph, edge_capacity_t>::type cap = get(edge_capacity, flow_g);
    typename property_map<FlowGraph, edge_residual_capacity_t>::type res_cap = get(edge_residual_capacity, flow_g);
    typename property_map<FlowGraph, edge_reverse_t>::type rev_edge = get(edge_reverse, flow_g);
    flow_graph_edge_descriptor e1, e2;
    edge_iterator ei, ei_end;
    bool inserted;

    flow_graph_edge_iterator fg_ei, fg_ei_end;
    typename graph_traits<FlowGraph>::in_edge_iterator fg_in_ei, fg_in_ei_end;
    flow_graph_vertex_iterator fg_vi, fg_vi_end;
    flow_graph_vertex_descriptor fg_u, fg_v, src, sink;
    typename graph_traits<FlowGraph>::adjacency_iterator fg_ai, fg_aend;
    typename std::vector<flow_graph_vertex_descriptor> fg_vd, fg_vd_new;
    typename std::vector<flow_graph_edge_descriptor> fg_ed;
    typename std::vector<flow_graph_edge_descriptor>::iterator fg_ed_i;

    /** Creating a Network Flow Graph out of the undirected graph **/

    /* 1.   Generate a directed graph from  the undirected input graph*/
    for (boost::tie(ei, ei_end) = edges(g); ei != ei_end; ei++)
    {
        u = source (*ei, g);
        v = target (*ei, g);
        boost::tie(e1, inserted) = add_edge(u, v, flow_g);
        fg_ed.push_back(e1);
        boost::tie(e2, inserted) = add_edge(v, u, flow_g);
        fg_ed.push_back(e2);
    }

    /* Create vectors for all the vertices originally in the graph and for new vertices added */
    for(boost::tie(fg_vi, fg_vi_end) = vertices(flow_g); fg_vi != fg_vi_end; fg_vi++)
    {
        fg_vd.push_back(*fg_vi);
        fg_u = add_vertex(flow_g);
        fg_vd_new.push_back(fg_u);
    }

    /* 2.   For every original vertex, create a new vertex and add out-edges from the original vertex to new vertex */
    /* 3.   Also add an out-edge between original vertex and new vertex  */
    for(unsigned i = 0; i<fg_vd.size(); i++)
    {
        for(boost::tie(fg_ei, fg_ei_end) = out_edges(fg_vd[i], flow_g); fg_ei != fg_ei_end; fg_ei++)
        {
            fg_u = target(*fg_ei, flow_g);
            add_edge(fg_vd_new[i], fg_u, flow_g);
        }
        add_edge(fg_vd[i], fg_vd_new[i], flow_g);
    }

    /* 4.   Remove all the out-edges originating from the original vertex except for the original to new vertex */
    for(fg_ed_i = fg_ed.begin(); fg_ed_i != fg_ed.end(); fg_ed_i++)
        remove_edge(*fg_ed_i, flow_g);

    /* 5.   Set capacities of all edges to 1 and declare a reverse edge for every edge to be passed as an argument to the max-flow algorithm */
    flow_graph_edge_iter fg_edge_iter, fg_edge_iter_end;
	for (boost::tie(fg_edge_iter, fg_edge_iter_end) = edges(flow_g); fg_edge_iter != fg_edge_iter_end; fg_edge_iter++)
	{
	    cap[*fg_edge_iter] = 1;
		fg_u = source(*fg_edge_iter, flow_g);
		fg_v = target(*fg_edge_iter, flow_g);
		boost::tie(e1,inserted) = add_edge(fg_v, fg_u, flow_g);
		cap[e1] = 0;
		rev_edge[*fg_edge_iter] = e1;
		rev_edge[e1] = *fg_edge_iter;
	}

    std::ofstream dot2("C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\flow_graph.dot");
    write_graphviz(dot2, flow_g);
    dot2.close();
	std::vector<default_color_type> color(num_vertices(flow_g));
    std::vector<flow_graph_edge_descriptor> pred(num_vertices(flow_g));

    /* Find the minimum degree vertex from the graph and its degree */
    boost::tie(p, delta) = detail2::min_degree_vertex(g);
    alpha_star = delta;
    detail2::neighbors(g, p, std::inserter(neighbor_S, neighbor_S.begin()));
    for (iter_x = neighbor_S.begin(); iter_x!=neighbor_S.end(); iter_x++)
    {
        for (iter_y = neighbor_S.begin(); iter_y != neighbor_S.end(); iter_y++)
        {
            x = *iter_x;
            y = *iter_y;
            if (x != y)
                {
                    boost::tie(g_e, inserted) = edge(x, y, g);
                    if (!inserted)
                        {
                            for (unsigned i = 0; i<fg_vd.size(); i++)
                                {
                                    if (fg_vd[i] == x)
                                    src = fg_vd_new[i];
                                    if (fg_vd[i] == y)
                                    sink = fg_vd[i];
                                }
                                alpha_S_k = edmonds_karp_max_flow (flow_g, src, sink, cap, res_cap, rev_edge, &color[0], &pred[0]);
                                if (alpha_S_k < alpha_star)
                                alpha_star = alpha_S_k;
                        }
                }
        }
    }

    neighbor_S.insert(p);
    boost::tie(vi, vi_end) = vertices(g);
    std::set_difference(vi, vi_end, neighbor_S.begin(), neighbor_S.end(), std::back_inserter(non_neighbor_S));

    while(!non_neighbor_S.empty())
    {
        k = non_neighbor_S.front();
        for (unsigned i = 0; i<fg_vd.size(); i++)
            {
                if (fg_vd[i] == p)
                src = fg_vd_new[i];
                if (fg_vd[i] == k)
                sink = fg_vd[i];
            }
        alpha_S_k = edmonds_karp_max_flow (flow_g, src, sink, cap, res_cap, rev_edge, &color[0], &pred[0]);
        if (alpha_S_k < alpha_star)
            alpha_star = alpha_S_k;
        non_neighbor_S.erase(non_neighbor_S.begin());
    }

    /* Finding the vertex disconnecting set */
	    /*struct greater_custom{
        template <typename T>
        bool operator()(T const & a, T const & b) const {return out_degree(a, g) > out_degree(b, g);}
        };*/
    neighbor_S.clear();
    detail2::neighbors(g, p, std::inserter(neighbor_S, neighbor_S.begin()));

	typename std::set<vertex_descriptor>::iterator si;

     if (alpha_star == delta)
            for (si = neighbor_S.begin(); si!=neighbor_S.end(); si++)
                *vertex_disconnecting_set++ = *si;
     /*else
    { degree_size_type degree = 0;
        //std::sort(neighbor_S.begin(), neighbor_S.end(), std::greater<vertex_descriptor>());
        for (si = neighbor_S.begin(); si != neighbor_S.end(); si++)
        {
            if (degree < alpha_star)
            {
            *vertex_disconnecting_set++ = *si;
            degree++;
            }
        }
}*/

    return alpha_star;
}
}
/* VERTEX_CONNECTIVITY_H_INCLUDED */
#endif
