#ifndef MATROID_MASTER_INCLUDED
#define MATROID_MASTER_INCLUDED

#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <algorithm>
#include <math.h>
#include <time.h>
#include <boost/multiprecision/cpp_int.hpp>
#include "Matroid_M2.h"
#include "Graph_Def.h"

using namespace std;
using namespace boost;
using boost::multiprecision::cpp_int;

vector<vector<graph_traits<digraph>::edge_descriptor>> matroid_master(digraph g, graph_traits<digraph>::vertex_descriptor root, int connectivity, vector<vector<graph_traits<digraph>::edge_descriptor>> independent_m2)
{
/********************************************************* Master Matroid Construction *******************************************************************/
	/* Defining number of out-edges of root node */
	int root_out_edges;
	root_out_edges = out_degree(root, g);

   int total_vertices = num_vertices(g);
   int num_nodes = (total_vertices - 1); // -1 because the subsets (Bi-Sets) are to be produced for all vertices of the graph except root vertex
   //cout<<"Number of vertices of the Graph (excluding the root vertex) are: "<<num_nodes<<endl;

	typedef graph_traits<digraph>::edge_descriptor ed;

	/** The Ground set of the master matroid is all edges of the input graph (excluding all edges with root node as the tail) 
	Now constructing the ground set of the Master Matroid: No out edge of the root vertex contributes in the ground set construction **/
	clear_out_edges(root, g);

	/* Create a DOT format file to later generate a visual representation of the graph */
	string output = "C:\\Users\\ggoel\\op_models\\Graph_Connectivity\\MPkCS\\Graph_Master.dot";
	ofstream dot(output);
    /* represent graph in DOT format and send to output file with a .dot extension */
    write_graphviz(dot, g);
    dot.close();
	/* Create a shell command to execute the dot.exe program that generates a graphics version of a DOT format graph */
	string command = "C:\\Users\\ggoel\\graphviz-2.36\\release\\bin\\dot -Kneato -Tjpg ";
	command.append(output);
	command.append(" -O");
	/* Execute the shell command */
	FILE *dot_output;
	dot_output = _popen(command.c_str(), "rt");
	_pclose(dot_output);


	/* Finding all Bi-Sets of the Vertex Set (not including the root vertex) */
	/* Create a 0-1 matrix to store the indices of the elements that form all possible subsets of the given base set */
	/* Create a 2D dynamic array to store the indices and initialize it with 0 */
	cout<<"Test0"<<endl;
	int** index;
	index = new int*[(int(pow(double(2), num_nodes)) - 1)];
	for (int i = 0; i < (int(pow(double(2), num_nodes)) - 1);  i++)
	{
		index[i] = new int[num_nodes];
	}
	cout<<"Test1"<<endl;
	for (int i = 0; i < (int(pow(double(2), num_nodes)) - 1);  i++)
	{
		for(int j = 0; j < num_nodes; j++)
		{
			index[i][j] = 0;
		}
	}
	cout<<"Test2"<<endl;
	/* Now represent 1 to (2^(num_nodes) - 1) in binary and store in the 2D array */
	int num;
	for (int i = 1; i < (int(pow(double(2), num_nodes)) - 0);  i++)
	{
		num = i;
		int j = 1;
		while(num >= 1)
		{
			int temp = num % 2;
			index[i-1][num_nodes - j] = temp;
			num = num / 2;
			j++;
		}
	}

	graph_traits<digraph>::vertex_iterator vi, vi_end;

	/*  Creating vectors to store elements of Outer and Inner Member of the Bi-Set X: x_o, x_i */
	typedef graph_traits<digraph>::vertex_descriptor vd;
	vector<vector<vd>> x_o((int(pow(double(2), num_nodes)) - 1));
	vector<vector<vector<vd>>> x_i;
	cout<<"Test3"<<endl;
	/* Entering all possible subsets of vertex set (not including root vertex) in x_o */
	for(int i = 0; i < (int(pow(double(2), num_nodes)) - 1); i++)
	{
		int j = 0;
		boost::tie(vi, vi_end) = vertices(g);
		while((j < num_nodes) && (vi != vi_end))
		{
			if ((*vi) != root)
			{
				if(index[i][j] == 1)
					{
						x_o[i].push_back(*vi);
					}
				j++;
			}
			vi++;
		}
	}
	delete [] index;
	cout<<"Test4"<<endl;
	/* Entering all possible subsets of "x_o" in x_i */
	for(int i = 0; i < (int(pow(double(2), num_nodes)) - 1); i++)
	{
		x_i.push_back(vector<vector<vd>>());
		int length = x_o[i].size();
		int** index2;
		index2 = new int*[(int(pow(double(2), length)) - 1)];

		for (int j = 0; j < (int(pow(double(2), length)) - 1);  j++)
			{
				index2[j] = new int[length];
			}
		
		for (int j = 0; j < (int(pow(double(2), length)) - 1);  j++)
		{
			for(int k = 0; k < length; k++)
			{
				index2[j][k] = 0;
			}
		}

		for(int j = 1; j <= (int(pow(double(2), length)) - 1); j++)
		{
			num = j;
			int k = 1;
			while(num >= 1)
			{
				int temp = num % 2;
				index2[j-1][length - k] = temp;
				num = num / 2;
				k++;
			}
		}
		
	/* Entering all possible subsets of "x_o" set in x_i */
	
	for(int j = 0; j < (int(pow(double(2), length)) - 1); j++)
	{
		x_i[i].push_back(vector<vd>());
		int k = 0;
		while(k < length)
		{
			if(index2[j][k] == 1)
			{
					x_i[i][j].push_back(x_o[i][k]);
			}
			k++;
		}
	}
	delete [] index2;
	}
	cout<<"Test5"<<endl;
	/** Now Creating the Master Matroid using a Bi-Set function B on the Bi-Set X = (X_o, X_i) created above 
	The Ground set of this matroid is all edges of the input (directed) graph minus all edges with root node as the tail **/
	cout<<"Test6"<<endl;
	/* Finding all possible subsets of edges in the directed graph */
	graph_traits<digraph>::edge_iterator ei, ei_end;
	int num_edges = 0;
	num_edges = boost::num_edges(g);
	unsigned cardinality_upper = (connectivity * (num_vertices(g) - 1)) - connectivity;
	unsigned cardinality_lower = ((connectivity * (num_vertices(g) - 1)) - root_out_edges);
	//unsigned cardinality_lower = ((connectivity * (num_vertices(g) - 1)) - connectivity);
	if (cardinality_lower == 0)
		cardinality_lower = 1;
	vector<vector<cpp_int>> edge_subset;
	int k = 0;
	for (unsigned i = cardinality_lower; i <= cardinality_upper; i++)
	{
		edge_subset.push_back(vector<cpp_int>());
		cpp_int s1 = (cpp_int(1) << i) - 1;
		while (!(s1 & cpp_int(1) << num_edges))
		{
			edge_subset[k].push_back(s1);
			cpp_int lo = s1 & ~(s1 - 1);       // lowest one bit
			cpp_int lz = (s1 + lo) & ~s1;      // lowest zero bit above lo
			s1 |= lz;                     // add lz to the set
			s1 &= ~(lz - 1);              // reset bits below lz
			s1 |= (lz / lo / 2) - 1;      // put back right number of bits at end
		}
		k++;
	}
	cout<<"Test7"<<endl;
	bool*** index3;
	index3 = new bool**[edge_subset.size()];
	for (size_t i = 0; i < edge_subset.size();  i++)
	{
		index3[i] = new bool*[edge_subset[i].size()];
		for (size_t j = 0; j < edge_subset[i].size(); j++)
		{
			index3[i][j] = new bool[num_edges];
		}
	}

	for (size_t i = 0; i < edge_subset.size();  i++)
	{
		for(size_t j = 0; j < edge_subset[i].size(); j++)
		{
			for (int k = 0; k < num_edges; k++)
				index3[i][j][k] = false;
		}
			
	}
	cout<<"Test8"<<endl;
	cpp_int num2 = 0;
	/* Now represent the contents of edge_subset 3D vector in binary and store in the 2D array */
	for (size_t i = 0; i < edge_subset.size();  i++)
	{
		for (size_t j = 0; j < edge_subset[i].size(); j++)
		{
			num2 = edge_subset[i][j];
			int k = 1;
			while(num2 >= 1)
			{
				cpp_int temp = num2 % 2;
				//index3[i][j][num_edges - k] = temp;
				if (temp == 1)
					index3[i][j][num_edges - k] = true;
				else
					index3[i][j][num_edges - k] = false;
				num2 = num2 / 2;
				k++;
			}
		}
	}

	vector<vector<vector<ed>>> edge_subsets;
	cout<<"Test9"<<endl;
	/* Entering all possible subsets of edges in edge_subsets */
	for (size_t k = 0; k < edge_subset.size(); k++)
	{
		edge_subsets.push_back(vector<vector<ed>>());
		for(size_t i = 0; i < edge_subset[k].size(); i++)
		{
			edge_subsets[k].push_back(vector<ed>());
			int j = 0;
			boost::tie(ei, ei_end) = edges(g);
			while((j < num_edges) && (ei != ei_end))
			{
				//if(index3[k][i][j] == 1)
				if(index3[k][i][j])
				{
					edge_subsets[k][i].push_back(*ei);
				}
				j++;
				ei++;
			}
		}
	}
	delete [] index3;

	/* Computing all independent subsets of the Master Matroid that satisfy the Independence Oracle */
	vector<vector<ed>> independent_master;
	graph_traits<digraph>::vertex_descriptor s, t, source1, source2, target1, target2;

	/* Create a temporary vector to store (x_o - x_i) */
	vector<vd>::iterator it1, it2;
  
	int bi_set_function = 0;
	int sum = 0;
	cout<<"Test10"<<endl;
	cout<<"Number of edge subsets (outer) are: "<<int(edge_subsets.size())<<endl;
	cout<<"Number of edge subsets are: "<<int(edge_subsets[0].size())<<endl;
	ofstream fout_count("edge_subset_count.txt");
	fout_count<<"Number of edge subsets (outer) are: "<<int(edge_subsets.size())<<endl;
	for (size_t i = 0; i < edge_subsets.size(); i++)
		fout_count<<int(edge_subsets[i].size())<<endl;
	//fout_count.close();
	int test_count = 0;
	for (size_t m = 0; m < edge_subsets.size(); m++)
	{
	for (size_t i = 0; i < edge_subsets[m].size(); i++)
	{
		test_count++;
		cout<<test_count<<endl;
		fout_count<<test_count<<endl;
		int subset_count = 0;
		for (size_t p = 0; p < independent_m2.size(); p++)
		{
			sum = 0;
			for (size_t n = 0; n < edge_subsets[m][i].size(); n++)
			{
				source1 = source(edge_subsets[m][i][n], g);
				target1 = target(edge_subsets[m][i][n], g);
				for (size_t q = 0; q < independent_m2[p].size(); q++)
				{
					source2 = source(independent_m2[p][q], g);
					target2 = target(independent_m2[p][q], g);
					if ((source1 == source2) && (target1 == target2))
					{
					sum++;
					break;
					}
				}
			}
			if (sum == edge_subsets[m][i].size())
				break;
			else
				subset_count++;
		}
		if (subset_count == independent_m2.size())
			continue;
		else
		{
			for (size_t k = 0; k < x_i.size(); k++)
			{
				for (size_t l = 0; l < x_i[k].size(); l++)
				{
					std::set<vd> s_x_o(x_o[k].begin(), x_o[k].end());
					std::set<vd> s_x_i(x_i[k][l].begin(), x_i[k][l].end());
					vector<vd> diff;
					std::set_difference( s_x_o.begin(), s_x_o.end(), s_x_i.begin(), s_x_i.end(), std::back_inserter( diff ) );
					bi_set_function = (connectivity*(x_i[k][l].size() - 1)) + diff.size();
					vector<ed> edges_induced;
					
					for (size_t j = 0; j < edge_subsets[m][i].size(); j++)
					{
						s = source(edge_subsets[m][i][j], g);
						t = target(edge_subsets[m][i][j], g);
						it1 = std::find(x_o[k].begin(), x_o[k].end(), s);
						it2 = std::find(x_i[k][l].begin(), x_i[k][l].end(), t);
						if ((it1 != x_o[k].end()) && (it2 != x_i[k][l].end()))
						edges_induced.push_back(edge_subsets[m][i][j]);
					}
					if (int(edges_induced.size()) > bi_set_function)
						goto END_OF_BISET_LOOPS;
				}
			}
			independent_master.push_back(edge_subsets[m][i]);
			END_OF_BISET_LOOPS:;
		}
	}
	}
	fout_count.close();
	cout<<"Master Matroid Construction Completed! Size is: "<<independent_master.size()<<endl;
	/****************************************************** Master Matroid Construction Ends *****************************************************************/
	return independent_master;
}
#endif